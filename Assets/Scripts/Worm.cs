﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;

public class Worm : MonoBehaviour
{
    [SerializeField] private List<GameObject> segmentWorms;
    private bool isPass;
    private float speed = 1;
    public Transform targetMove;
    [SerializeField] Rigidbody2D rb;
    [SerializeField] Vector2 currTranform;
    public void AddSegmentWorm(GameObject segment)
    {
        segmentWorms.Insert(1, segment);
    }
    public  void Move()
    {
        List<Vector3> currTranforms = new List<Vector3>();
        for (int i= 0; i <segmentWorms.Count; i++)
        {
            Vector3 v = segmentWorms[i].transform.localPosition;
            currTranforms.Add(v);
        }
        for (int i = segmentWorms.Count - 1; i >= 1; i--)
        {
            MoveAnim(segmentWorms[i], currTranforms[i-1]);
        }
        for (int i = 1; i < segmentWorms.Count; i++)
        {
            if (i != 1)
            {
                Vector3 dir = segmentWorms[i - 1].transform.localPosition - segmentWorms[i].transform.localPosition;
                Vector2Int direction = ConvertVector3ToVector2Int(dir);
                segmentWorms[i].transform.eulerAngles = new Vector3(0, 0, GetRotationEuler(direction));
            }
            else
            {
                Vector3 dir = segmentWorms[i].transform.localPosition - segmentWorms[i + 1].transform.localPosition;
                Vector2Int direction = ConvertVector3ToVector2Int(dir);
                segmentWorms[i].transform.eulerAngles = new Vector3(0, 0, GetRotationEuler(direction));
            }
        }

    }
    private async void MoveAnim(GameObject curr, Vector3 next)
    {
        var init = curr.transform.localPosition;
        float time = 0.2f;
        float passed = 0f;
        while (passed < time)
        {
            passed += Time.deltaTime;
            var normalize = passed / time;
            var current = Vector3.Lerp(init, next, normalize);
            curr.transform.localPosition = current;
            await Task.Yield();
        }
    }
    Vector2Int ConvertVector3ToVector2Int(Vector3 vector3)
    {
        // Lấy giá trị x và y của Vector3, và chuyển đổi chúng thành int
        int x = Mathf.RoundToInt(vector3.x);
        int y = Mathf.RoundToInt(vector3.y);

        // Tạo và trả về Vector2Int mới
        return new Vector2Int(x, y);
    }
    public List<GameObject> GetSegments()
    {
        return segmentWorms;
    }
    public float GetRotationEuler(Vector2Int dir)
    {
        if (dir == new Vector2Int(1, 0))
        {
            return 0;
        }
        else if (dir == new Vector2Int(-1, 0))
        {
            return -180;
        }
        else if (dir == new Vector2Int(0, 1))
        {
            return 90;
        }
        else if (dir == new Vector2Int(0, -1))
        {
            return -90;
        }
        else
        {
            return 0;
        }
    }
    public void MovePass()
    {
        isPass = true;
        transform.GetComponent<Rigidbody2D>().isKinematic = true;
    }

    private void Update()
    {
        
        if (rb.velocity.y < 0)
        {
            transform.position = new Vector3(currTranform.x, transform.position.y, 0);
        }
    }
}
